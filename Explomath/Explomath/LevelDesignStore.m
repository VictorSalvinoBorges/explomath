//
//  LevelDesignStore.m
//  Explomath
//
//  Created by Victor Salvino Borges on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "LevelDesignStore.h"
#import "FileManager.h"
#import "LevelDesign.h"

#define LEVEL_DESIGN_FILE @"LevelDesign.json"
NSString *const levelDesignFile = LEVEL_DESIGN_FILE;

@implementation LevelDesignStore{
    NSMutableArray *_levelDesign;
}

+(LevelDesignStore *)sharedInstance{
    static LevelDesignStore *_sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[super alloc] init];
    });
    
    return _sharedInstance;
}

- (id) init {
    self = [super init];
    if (self) {
        _levelDesign = [[NSMutableArray alloc] init];
        [self readLevel];
    }
    return self;
}

- (void)readLevel {
    /*NSError *error;
    NSData *jsonData = [NSData dataWithContentsOfFile:[FileManager getPathFromDocuments:levelDesignFile]];
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    if (!data) {
        NSLog(@"Error loading level status: %@", error);
    }
    
    NSArray *jLevelDesign = [data objectForKey:@"levelDesign"];
    
    for (NSDictionary *l in jLevelDesign) {
        int level = [[l objectForKey:@"level"] intValue];
        int operacao = [[l objectForKey:@"operacao"] intValue];
        NSString *achievement = [l objectForKey:@"achievement"];
        int extra = [[l objectForKey:@"extras"] intValue];
        
        LevelDesign *levelDesign = [[LevelDesign alloc] initLevelDesignWith:level :operacao :achievement :extra];
        
        [_levelDesign addObject:levelDesign];
    }*/
}

@end
