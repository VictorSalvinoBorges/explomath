//
//  Constants.h
//  THLabelExample
//
//  Created by Tobias Hagemann on 09/09/14.
//  Copyright (c) 2014 tobiha.de. All rights reserved.
//

#define kShadowColor1		[UIColor blackColor]
#define kShadowColor2		[UIColor colorWithWhite:0.0 alpha:0.75]
#define kShadowOffset		CGSizeMake(0.0, UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 4.0 : 2.0)
#define kShadowBlur			(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 10.0 : 5.0)
#define kInnerShadowOffset	CGSizeMake(0.0, UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 2.0 : 1.0)
#define kInnerShadowBlur	(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 4.0 : 2.0)

#define kStrokeColor		[UIColor colorWithRed:104.0/255.0 green:55.0/255.0 blue:12.0/255.0 alpha:1.0]
#define kStrokeColor2		[UIColor colorWithRed:46.0/255.0 green:114.0/255.0 blue:165.0/255.0 alpha:1.0]
#define kStrokeSize			(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 5.0 : 2.0)
#define kStrokeSize2			(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 8.0 : 4.0)
#define kStrokeSize3			(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 3.0 : 1.0)

#define kGradientStartColor	[UIColor colorWithRed:255.0 / 255.0 green:193.0 / 255.0 blue:127.0 / 255.0 alpha:1.0]
#define kGradientEndColor	[UIColor colorWithRed:255.0 / 255.0 green:163.0 / 255.0 blue:64.0 / 255.0 alpha:1.0]
