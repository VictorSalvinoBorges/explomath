//
//  SoundManager.m
//  Explomath
//
//  Created by Victor Salvino Borges on 23/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "SoundManager.h"
#import <AVFoundation/AVFoundation.h>
#import "Sound.h"


@implementation SoundManager{
    NSMutableArray *_soundsManager;
}

+(SoundManager *)sharedInstance{
    static SoundManager *_sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[super alloc] init];
    });
    
    return _sharedInstance;
}

-(instancetype)init{;
    self = [super init];
    if (self) {
        _soundsManager = [[NSMutableArray alloc] init];
        [self inicializarSons];
    }
    return self;
}

-(void)inicializarSons{
    NSString *path = [NSString stringWithFormat:@"%@/backgroundSound.wav", [[NSBundle mainBundle]resourcePath]];
    NSURL *soundUrlBackground = [NSURL fileURLWithPath:path];
    
    path = [NSString stringWithFormat:@"%@/rightAnswer.wav", [[NSBundle mainBundle]resourcePath]];
    NSURL *soundUrlRight = [NSURL fileURLWithPath:path];
    
    path = [NSString stringWithFormat:@"%@/wrongAnswer.wav", [[NSBundle mainBundle]resourcePath]];
    NSURL *soundUrlWrong = [NSURL fileURLWithPath:path];
    
    path = [NSString stringWithFormat:@"%@/bomb.wav", [[NSBundle mainBundle]resourcePath]];
    NSURL *soundUrlBomb = [NSURL fileURLWithPath:path];
    
    path = [NSString stringWithFormat:@"%@/timer15.wav", [[NSBundle mainBundle]resourcePath]];
    NSURL *soundUrlTimer = [NSURL fileURLWithPath:path];
    
    Sound *tempSounds = [[Sound alloc] initSoundsBackgroundWith:soundUrlBackground UrlRight:soundUrlRight UrlWrong:soundUrlWrong UrlBomb:soundUrlBomb UrlTimer:soundUrlTimer];
    
    [_soundsManager addObject:tempSounds];
}

-(void)configBackgroundSound{
    
    [[_soundsManager objectAtIndex:0] sndBackground].numberOfLoops = -1;
    //[[_soundsManager objectAtIndex:0] sndTimer].numberOfLoops = -1;
    
}

- (void)setSoundOnOff{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *tempOnOff = [defaults objectForKey:@"SoundEnable"];
    
    if ([tempOnOff isEqualToString:@"0"]) {
        [[[_soundsManager objectAtIndex:0] sndBackground] setVolume:0];
        [[[_soundsManager objectAtIndex:0] sndRight] setVolume:0];
        [[[_soundsManager objectAtIndex:0] sndWrong] setVolume:0];
        [[[_soundsManager objectAtIndex:0] sndBomb] setVolume:0];
        [[[_soundsManager objectAtIndex:0] sndTimer] setVolume:0];
    }else{
        [[[_soundsManager objectAtIndex:0] sndBackground] setVolume:1];
        [[[_soundsManager objectAtIndex:0] sndRight] setVolume:1];
        [[[_soundsManager objectAtIndex:0] sndWrong] setVolume:1];
        [[[_soundsManager objectAtIndex:0] sndBomb] setVolume:1];
        [[[_soundsManager objectAtIndex:0] sndTimer] setVolume:1];
    }
    
}

- (void)playBackgroundSound:(BOOL)blPlay{
    
    if (blPlay == YES) {
        [[[_soundsManager objectAtIndex:0] sndBackground] play];
    }else{
        [[[_soundsManager objectAtIndex:0] sndBackground] stop];
    }
}

- (void)playRightSound:(BOOL)blPlay{
    if (blPlay == YES) {
        [[[_soundsManager objectAtIndex:0] sndRight] setCurrentTime:0.0];
        [[[_soundsManager objectAtIndex:0] sndRight] play];
    }else{
        [[[_soundsManager objectAtIndex:0] sndRight] stop];
    }
}

- (void)playWrongSound:(BOOL)blPlay{
    if (blPlay == YES) {
        [[[_soundsManager objectAtIndex:0] sndWrong] setCurrentTime:0.0];
        [[[_soundsManager objectAtIndex:0] sndWrong] play];
    }else{
        [[[_soundsManager objectAtIndex:0] sndWrong] stop];
    }
}

- (void)playBombSound:(BOOL)blPlay{
    if (blPlay == YES) {
        [[[_soundsManager objectAtIndex:0] sndBomb] setCurrentTime:0.0];
        [[[_soundsManager objectAtIndex:0] sndBomb] play];
    }else{
        [[[_soundsManager objectAtIndex:0] sndBomb] stop];
    }
}

- (void)playTimer:(BOOL)blPlay{
    if (blPlay == YES) {
        [[[_soundsManager objectAtIndex:0] sndTimer]  setCurrentTime:0.0];
        [[[_soundsManager objectAtIndex:0] sndTimer] play];
    }else{
        [[[_soundsManager objectAtIndex:0] sndTimer] stop];
    }
}

-(void)stopAllSoundsEffects{
    [[[_soundsManager objectAtIndex:0] sndRight] stop];
    [[[_soundsManager objectAtIndex:0] sndWrong] stop];
    [[[_soundsManager objectAtIndex:0] sndBomb] stop];
    [[[_soundsManager objectAtIndex:0] sndTimer] stop];
}



@end
