//
//  EndLevelViewController.m
//  Explomath
//
//  Created by Matheus Cassol on 27/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "EndLevelViewController.h"
#import "THLabel.h"
#import "Constants.h"
#import "Util.h"
#import "LevelViewController.h"

@interface EndLevelViewController ()

@property (weak, nonatomic) IBOutlet UIButton *btnPlanetaNatal;
@property (weak, nonatomic) IBOutlet UIButton *btnOtherPlanet;
@property (weak, nonatomic) IBOutlet UIImageView *imgStars;
@property (weak, nonatomic) IBOutlet UIImageView *imgReward;

@property (weak, nonatomic) IBOutlet THLabel *thLblConquista;
@property (weak, nonatomic) IBOutlet THLabel *thLblMensagem;
@property (weak, nonatomic) IBOutlet THLabel *thLblPlanetaNatal;
@property (weak, nonatomic) IBOutlet THLabel *thLblOtherPlanet;
@property (weak, nonatomic) IBOutlet THLabel *thLblOperation;

@end

@implementation EndLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self changeFont];
    [self internationalization];
}

- (void)viewWillAppear:(BOOL)animated{
    if (_stars != 0 ) {
        NSString *rewardImg = [NSString stringWithFormat:@"reward%d.png", _completedLevel];
        _imgReward.image = [UIImage imageNamed:rewardImg];
    } else {
        
    }
    
    NSString *starsImg = [NSString stringWithFormat:@"estrelas%d.png", _stars];
    _imgStars.image = [UIImage imageNamed:starsImg];
    
    [self internationalization];
}

// This function will change all components name to current language
- (void)internationalization {
    if (_stars != 0) {
        [_thLblOperation setHidden:YES];
        [_imgReward setHidden:NO];
        _thLblConquista.text = NSLocalizedStringFromTableInBundle(@"ACHIEVEMENT", nil, [Util sharedInstance].localeBundle, nil);
        _thLblMensagem.text = NSLocalizedStringFromTableInBundle(@"YOU_RESOLVED_ALL", nil, [Util sharedInstance].localeBundle, nil);
    } else {
        [_thLblOperation setHidden:NO];
        _thLblOperation.text = _expression;
        _thLblConquista.text = NSLocalizedStringFromTableInBundle(@"TRY_AGAIN", nil, [Util sharedInstance].localeBundle, nil);
        _thLblMensagem.text = NSLocalizedStringFromTableInBundle(@"OH_NO", nil, [Util sharedInstance].localeBundle, nil);
        [_btnOtherPlanet setEnabled:NO];
        [_imgReward setHidden:YES];
    }
}

- (void) changeFont {
    _thLblOperation.font = [UIFont fontWithName:@"Skia-Regular_Black" size:35];
    _thLblOperation.strokeColor = kStrokeColor;
    _thLblOperation.strokeSize = kStrokeSize;
    
    _thLblConquista.font = [UIFont fontWithName:@"Skia-Regular_Black" size:35];
    _thLblConquista.strokeColor = kStrokeColor;
    _thLblConquista.strokeSize = kStrokeSize;
    
    _thLblMensagem.font = [UIFont fontWithName:@"Skia-Regular_Black" size:20];
    _thLblMensagem.strokeColor = kStrokeColor;
    _thLblMensagem.strokeSize = kStrokeSize3;
    _thLblMensagem.text = @"Você resolveu todos os problemas deste nível!\nParabéns!";
    
    _thLblPlanetaNatal.font = [UIFont fontWithName:@"Skia-Regular_Black" size:17];
    _thLblPlanetaNatal.strokeColor = kStrokeColor;
    _thLblPlanetaNatal.strokeSize = kStrokeSize3;
    
    _thLblOtherPlanet.font = [UIFont fontWithName:@"Skia-Regular_Black" size:17];
    _thLblOtherPlanet.strokeColor = kStrokeColor;
    _thLblOtherPlanet.strokeSize = kStrokeSize3;
}

- (IBAction)btnPlanetaNatalClick:(id)sender {
    
    [(LevelViewController*)[self presentingViewController] resetLevel];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnNextLevel:(id)sender {
    
    if (_lvl < 5) {
        [(LevelViewController*)[self presentingViewController] setLvl:_lvl+1];
        [(LevelViewController*)[self presentingViewController] resetLevel];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
