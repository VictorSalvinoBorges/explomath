//
//  RascunhoViewController.m
//  Explomath
//
//  Created by Bruna Matos on 20/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "SketchViewController.h"
#import "LevelViewController.h"
#import "THLabel.h"
#import "Constants.h"
#import "Util.h"

@interface SketchViewController (){
    NSString *_expressao;   // variavel correspondente a expressao
    int timeToResponse;     //tempo restante para responder
    
}
@property (strong, nonatomic) IBOutlet UIView *rascunhoView;
@property (weak, nonatomic) IBOutlet UILabel *lblRasc1;
@property (weak, nonatomic) IBOutlet UILabel *lblExpressao;

// Contornos
@property (weak, nonatomic) IBOutlet THLabel *lblCRasc;
// variavel time
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@end

@implementation SketchViewController{
    CGPoint startPoint;
}

- (void)viewDidLoad
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    drawImage.image = [defaults objectForKey:@"drawImageKey"];
    drawImage = [[UIImageView alloc] initWithImage:nil];
    drawImage.frame = _rascunhoView.frame;
    [_rascunhoView addSubview:drawImage];
    _lblRasc1.text = _expressao;
    

    
    [self changeFont];
    [super viewDidLoad];
    [self internationalization];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated{
    _lblRasc1.text = _expressao;
    // seta variaveis de tempo
    timeToResponse = _tempoFalta;
    _lblTime.text = @"";
    if (_trSet) {
        [self startTimer];
    }
    
    [self internationalization];
}


-(void)startTimer{
    
    _lblTime.text = [NSString stringWithFormat:@"%ds", timeToResponse];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                              target:self
                                            selector:@selector(timePlaying)
                                            userInfo:nil
                                             repeats: YES];
    
}

- (void)timePlaying {
    
    timeToResponse--;
    
    _lblTime.text = [NSString stringWithFormat:@"%ds", timeToResponse];
    
    
    
    if (timeToResponse == 0) {
        if(_timer)
        {
            [_timer invalidate];
            _timer = nil;
            timeToResponse = _tempoFalta;
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }
}

-(void)timerSet{

    // setar a boleana do timer para indicar na proxima view que ele esta ativado
    [Util sharedInstance].trSetR = YES;
    [Util sharedInstance].timerValueR = timeToResponse;
    [Util sharedInstance].expRasc = _lblExpressao.text;
}

- (void) changeFont {

    _lblCRasc.font = [UIFont fontWithName:@"Skia-Regular_Black" size:40];
    _lblCRasc.strokeColor = kStrokeColor;
    _lblCRasc.strokeSize = kStrokeSize;
    
    _lblExpressao.font = [UIFont fontWithName:@"Skia-Regular_Black" size:30];
    
}

// This function will change all components name to current language
- (void)internationalization {
    // label Oxigenio
    _lblCRasc.text = NSLocalizedStringFromTableInBundle(@"SKETCH", nil, [Util sharedInstance].localeBundle, nil);
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([touch tapCount] == 2) {
        drawImage.image = nil;
    }
    
    
    location = [touch locationInView:touch.view];
    lastClick = [NSDate date];
    
    lastPoint = [touch locationInView:_rascunhoView];
    lastPoint.y -= 0;
    
    
    [super touchesBegan: touches withEvent: event];
    
}



- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    mouseSwiped = YES;
    
    UITouch *touch = [touches anyObject];
    currentPoint = [touch locationInView:_rascunhoView];
    
    UIGraphicsBeginImageContext(CGSizeMake(335, 580));
    [drawImage.image drawInRect:CGRectMake(0, 0, 335, 580)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 5.0);
    
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    
    
    [drawImage setFrame:CGRectMake(0, 0, 335, 580)];
    drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    lastPoint = currentPoint;
    
    [_rascunhoView addSubview:drawImage];
}

- (IBAction)btnVoltarRascunho:(id)sender {
    [self timerSet];
    [self dismissViewControllerAnimated:YES completion:nil];

}


@end
