//
//  LevelStatusStore.m
//  Explomath
//
//  Created by Cristian Simioni Milani on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "LevelStatusStore.h"
#import "FileManager.h"
#import "LevelStatus.h"

#define LEVEL_STATUS_FILE @"LevelStatus.json"
NSString *const levelStatusFile = LEVEL_STATUS_FILE;

@implementation LevelStatusStore {
    NSMutableArray *_levelsStatus;
}

+ (LevelStatusStore *)sharedInstance {
    static LevelStatusStore *_sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[super alloc] init];
    });
    
    return _sharedInstance;
}

- (id) init {
    self = [super init];
    if (self) {
        _levelsStatus = [[NSMutableArray alloc] init];
        [self readStatus];
    }
    return self;
}

- (void)readStatus {
    NSError *error;
    NSData *jsonData = [NSData dataWithContentsOfFile:[FileManager getPathFromDocuments:levelStatusFile]];
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    if (!data) {
        NSLog(@"Error loading level status: %@", error);
    }
    
    NSArray *jLevelStatus = [data objectForKey:@"status"];
    [_levelsStatus removeAllObjects];
    for (NSDictionary *l in jLevelStatus) {
        int level = [[l objectForKey:@"level"] intValue];
        BOOL blocked = [[l objectForKey:@"blocked"] intValue] == 1 ? YES : NO;
        BOOL complete = [[l objectForKey:@"complete"] intValue] == 1 ? YES : NO;
        int stars = [[l objectForKey:@"stars"] intValue];
        int planet = [[l objectForKey:@"planet"] intValue];
        
        LevelStatus *levelStatus = [[LevelStatus alloc] initLevelStatusWith:level :blocked :complete :stars :planet];
        
        [_levelsStatus addObject:levelStatus];
    }
}

- (void)updateStatus:(LevelStatus*)level {
    NSError *error;
    NSData *jsonData = [NSData dataWithContentsOfFile:[FileManager getPathFromDocuments:levelStatusFile]];
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    if (!data) {
        NSLog(@"Error loading level status: %@", error);
    }
    
    NSMutableArray *jLevelStatus = [[data objectForKey:@"status"] mutableCopy];
    
    int i = 0;
    for (NSMutableDictionary *l in jLevelStatus) {
        if ([[l objectForKey:@"level"] intValue] == level.level) {
            if (!level.blocked) {
                [l setObject:@"0" forKey:@"blocked"];
            } else {
                [l setObject:@"1" forKey:@"blocked"];
            }
            if (!level.complete) {
                [l setObject:@"0" forKey:@"complete"];
            } else {
                [l setObject:@"1" forKey:@"complete"];
            }
            [l setObject:[NSString stringWithFormat:@"%d", level.stars] forKey:@"stars"];
            [_levelsStatus removeObjectAtIndex:i];
            [_levelsStatus insertObject:level atIndex:i];
            break;
        }
        i++;
    }
    
    NSDictionary *levelsStatusDictionary = [NSDictionary dictionaryWithObject:jLevelStatus forKey:@"status"];
    
    jsonData = [NSJSONSerialization dataWithJSONObject:levelsStatusDictionary options:0 error:&error];
    [jsonData writeToFile:[FileManager getPathFromDocuments:levelStatusFile] atomically:YES];

}

- (int) numberOfLevelsClosedFor:(int)planet {
    int quantity = 0;
    
    for (LevelStatus *l in _levelsStatus) {
        if (l.planet == planet && l.complete)
            quantity++;
    }
    
    return quantity;
}

- (int) lastLevel {
    int level = 0;
    
    for (int i = 0; i < _levelsStatus.count; i++) {
        LevelStatus *l = [_levelsStatus objectAtIndex:i];
        if (l.complete) {
            level = l.level;
        }
    }
    
    return level;
}

- (int) lastPlanet {
    int planet = 1;
    
    for (int i = 0; i < _levelsStatus.count; i++) {
        LevelStatus *l = [_levelsStatus objectAtIndex:i];
        if (!l.complete) {
            planet = l.planet;
        }
    }
    
    return planet;
}

- (BOOL) isBlocked:(int) planet :(int) level {
    for(LevelStatus *l in _levelsStatus) {
        if (l.planet == planet && l.level == level) {
            return l.blocked;
        }
    }
    return NO;
}

-(int)numberOfStarsInLevel:(int)lvl{
    LevelStatus *lvlS = _levelsStatus[lvl-1];
    return lvlS.stars;
}

-(BOOL)isComplete:(int)planet :(int)lvl{
    
    for (LevelStatus* level in _levelsStatus) {
        if (level.planet == planet && level.level == lvl) {
            return level.complete;
        }
    }
    return NO;
}

@end
