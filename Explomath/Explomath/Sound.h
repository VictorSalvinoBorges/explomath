//
//  Sound.h
//  Explomath
//
//  Created by Victor Salvino Borges on 23/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface Sound : NSObject

@property (nonatomic, strong) AVAudioPlayer* sndBackground;
@property (nonatomic, strong) AVAudioPlayer* sndRight;
@property (nonatomic, strong) AVAudioPlayer* sndWrong;
@property (nonatomic, strong) AVAudioPlayer* sndBomb;
@property (nonatomic, strong) AVAudioPlayer* sndTimer;

- (id) initSoundsBackgroundWith:(NSURL*)urlBackgroundSnd UrlRight:(NSURL*)urlRightSnd UrlWrong:(NSURL*)urlWrongSnd UrlBomb:(NSURL*)urlBombSnd UrlTimer:(NSURL*)urlTimerSnd;

-(void)resetTimerSound:(NSURL *)urlTimerSnd;

-(void)resetRightSound:(NSURL *)urlTimerSnd;

-(void)resetWrongSound:(NSURL *)urlTimerSnd;

-(void)resetBombSound:(NSURL *)urlTimerSnd;

@end
