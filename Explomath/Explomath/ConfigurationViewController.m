//
//  ConfigurationViewController.m
//  Explomath
//
//  Created by Cristian Simioni Milani on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "ConfigurationViewController.h"
#import "FileManager.h"
#import "LevelStatusStore.h"
#import "LevelStatus.h"
#import "SoundManager.h"
#import "Sound.h"
#import "Util.h"
#import "THLabel.h"
#import "Constants.h"

@interface ConfigurationViewController (){
    SoundManager *sounds;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLanguage;
@property (weak, nonatomic) IBOutlet UILabel *lblSound;
@property (weak, nonatomic) IBOutlet UIButton *lblAbout;
@property (weak, nonatomic) IBOutlet UIButton *lblResetProgress;
// Stroke
@property (weak, nonatomic) IBOutlet THLabel *sblOptions;
@property (weak, nonatomic) IBOutlet THLabel *slblLanguage;
@property (weak, nonatomic) IBOutlet THLabel *slblSound;

@end

@implementation ConfigurationViewController

@synthesize segCtrlLanguage;
@synthesize swtSom;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self internationalization];
    [self changeFont];
    
    segCtrlLanguage.selectedSegmentIndex = [self getDefaultLanguage];
    [swtSom setOn: [ self getDefaultSound]];
}

-(void)viewWillAppear:(BOOL)animated{
    [self internationalization];
}

- (void)changeFont {
    _lblTitle.font = [UIFont fontWithName:@"Skia-Regular_Black" size:50];
    _sblOptions.strokeColor = kStrokeColor;
    _sblOptions.strokeSize = kStrokeSize;
    
    _lblLanguage.font = [UIFont fontWithName:@"Skia-Regular_Black" size:30];
    _slblLanguage.strokeColor = kStrokeColor;
    _slblLanguage.strokeSize = kStrokeSize;
    
    _lblSound.font = [UIFont fontWithName:@"Skia-Regular_Black" size:30];
    _slblSound.strokeColor = kStrokeColor;
    _slblSound.strokeSize = kStrokeSize;
    
    [_lblAbout.titleLabel setFont:[UIFont fontWithName:@"Skia-Regular_Black" size:20]];
    [_lblResetProgress.titleLabel setFont:[UIFont fontWithName:@"Skia-Regular_Black" size:25]];
}

- (void)internationalization {
    _lblTitle.text = NSLocalizedStringFromTableInBundle(@"OPTIONS", nil, [Util sharedInstance].localeBundle, nil);
    _lblLanguage.text = NSLocalizedStringFromTableInBundle(@"LANGUAGE", nil, [Util sharedInstance].localeBundle, nil);
    _lblSound.text = NSLocalizedStringFromTableInBundle(@"SOUND", nil, [Util sharedInstance].localeBundle, nil);
    [_lblAbout setTitle:NSLocalizedStringFromTableInBundle(@"ABOUT", nil, [Util sharedInstance].localeBundle, nil) forState:UIControlStateNormal];
    [_lblResetProgress setTitle:NSLocalizedStringFromTableInBundle(@"RESET_PROGRESS", nil, [Util sharedInstance].localeBundle, nil) forState:UIControlStateNormal];
    // Portuguese
    [segCtrlLanguage setTitle:NSLocalizedStringFromTableInBundle(@"PORTUGUESE", nil, [Util sharedInstance].localeBundle, nil) forSegmentAtIndex:0];
    // English
    [segCtrlLanguage setTitle:NSLocalizedStringFromTableInBundle(@"ENGLISH", nil, [Util sharedInstance].localeBundle, nil) forSegmentAtIndex:1];
}

- (IBAction)btnResetProgress:(id)sender {
    
    NSString *resetProgress = NSLocalizedStringFromTableInBundle(@"RESET_PROGRESS", nil, [Util sharedInstance].localeBundle, nil);
    NSString *msgAlert = NSLocalizedStringFromTableInBundle(@"MSG_RESET_DATA", nil, [Util sharedInstance].localeBundle, nil);
    NSString *cancel = NSLocalizedStringFromTableInBundle(@"CANCEL", nil, [Util sharedInstance].localeBundle, nil);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:resetProgress message:msgAlert preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
    
        // Delete current LevelStatus.json file
        [[NSFileManager defaultManager] removeItemAtPath:[FileManager getPathFromDocuments:@"LevelStatus.json"] error:nil];
        
        // Move the LevelStatus.json from Bundle to Documents
        [FileManager moveFromBundleToDocuments:levelStatusFile];
        [[LevelStatusStore sharedInstance] readStatus];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
        
    }];
    
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)btnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changeLanguage:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString * language;
    
    if (segCtrlLanguage.selectedSegmentIndex == 0) {
        [defaults setObject:@"pt" forKey:@"LanguageSelected"];
        language = @"pt";
    }else{
        [defaults setObject:@"en" forKey:@"LanguageSelected"];
        language = @"en";
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    if (path) {
        [Util sharedInstance].localeBundle = [NSBundle bundleWithPath:path];
    }
    else {
        [Util sharedInstance].localeBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"] ];
    }
    
    [self internationalization];
}

- (IBAction)soundControl:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([swtSom isOn]) {
        [defaults setObject:@"1" forKey:@"SoundEnable"];
    }else{
        [defaults setObject:@"0" forKey:@"SoundEnable"];
    }
    
    sounds = [SoundManager sharedInstance];
    [sounds setSoundOnOff];
}

-(BOOL)getDefaultSound {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *sound = [defaults stringForKey:@"SoundEnable"];
    if ([sound isEqual: @"0"]) {
        return NO;
    } else {
        return YES;
    }
}

-(int)getDefaultLanguage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *language = [defaults stringForKey:@"LanguageSelected"];
    if ([language isEqual:@"en"]) {
        return 1;
    } else {
        return 0;
    }
}

@end
