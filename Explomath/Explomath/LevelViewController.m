//
//  LevelViewController.m
//  Explomath
//
//  Created by Matheus Cassol on 20/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "LevelViewController.h"
#import "MatrixCell.h"
#import "SketchViewController.h"
#import "LevelDesign.h"
#import "THLabel.h"
#import "Constants.h"
#import "Util.h"
#import "ConfigurationViewController.h"
#import "SoundManager.h"
#import "Sound.h"
#import "User.h"
#import "LevelStatus.h"
#import "LevelStatusStore.h"
#import "EndLevelViewController.h"

@interface LevelViewController (){
    UIButton* btnsMatrix[5][5];      //matriz de botoes
    MatrixCell* cellsMatrix[5][5];
    
    SEL selClickCell;                //selector que recebera a acao das celulas
    SEL selClickKeyboard;            //selector que recebera a acao dos botoes do keyboard
    
    int coef;                        //coeficiente de dificuldade do level para randomizar os numeros
    int resposta;                           //resposta da celula sendo preenchida
    int iAtual, jAtual;              //i e j da celula atualmente selecionada
    int oxigenio;                    //quantidade de oxigenio atual
    int timeToResponse;              //tempo restante para responder
    int timeToRascunho;              // variavel de tempo que sera enviado para a view do rascunho
    
    NSString *respostaAtual;         //resposta digitada pelo usuario
    NSString *exp;
    NSString *tipoCell;                     //tipo da celula sendo preenchida
    
    LevelDesign *lvlD;
    

    BOOL timerSet;                   // variavel para indicar que o timer esta setado
    
    SoundManager *sounds;
    
    int nAnswers;
    
}
// array das celulas
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cells;
// label da expressao
@property (weak, nonatomic) IBOutlet UILabel *lblExpression;
// array de botoes de entrada do usuario
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *keyboard;
// label de respostas
@property (weak, nonatomic) IBOutlet UILabel *lblAnswer;
// variavel time
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
// oxigenio
@property (weak, nonatomic) IBOutlet UIImageView *imgOxigenio;
// botao mais/menos para inserir sinal negativo
@property (weak, nonatomic) IBOutlet UIButton *btnSinal;
// Imagem para representar o timer
@property (weak, nonatomic) IBOutlet UIImageView *imgTimer;

// Contorno dos labels
@property (weak, nonatomic) IBOutlet THLabel *lblOxigenio;
@property (weak, nonatomic) IBOutlet THLabel *lblConta;
@property (weak, nonatomic) IBOutlet THLabel *lblResposta;



@end

@implementation LevelViewController{
    // valor da linha da celula selecionada
    int iShare;
    // valor da coluna da celula selecionada
    int jShare;
}

- (void)viewDidLoad {
    [self changeFont];
    [super viewDidLoad];
    [self internationalization];
    [self checkTimerRascunho];
    
    // seta as variaveis do shared instance
    iShare = [Util sharedInstance].icell;
    jShare = [Util sharedInstance].jcell;
    [self initGame];
    sounds = [SoundManager sharedInstance];
    nAnswers = 0;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self internationalization];
    [self eraseScreen];
    [self checkTimerRascunho];
}

-(void)viewWillDisappear:(BOOL)animated{
    [sounds playTimer:NO];
}

-(void)initGame{
    oxigenio = 4;
    coef = _lvl*3 + 6;
    timeToResponse = 15;
    
    [self setLvlDesign];
    
    selClickCell = @selector(btnCellClick:);
    selClickKeyboard = @selector(btnKeyboardClick:);
    
    [self fillBtnsMatrix];
    [self fillCellsMatrix];
    [self setCellsImgs];
    [self setKeyboardBtns];
    [self eraseScreen];
    [self setImageOxygen];
}

-(void)checkTimerRascunho{
    if ([Util sharedInstance].trSetR) {
        timeToResponse = [Util sharedInstance].timerValueR;
        _lblExpression.text = [Util sharedInstance].expRasc;
        
        if ([cellsMatrix[iShare][jShare].type isEqualToString:@"timer"]) {
            [btnsMatrix[iShare][jShare] setImage:[UIImage imageNamed:@"clock.png"] forState:UIControlStateNormal];
            [self disableOtherCells];
        }
        else if([cellsMatrix[iShare][jShare].type isEqualToString:@"bomba"]){
            [btnsMatrix[iShare][jShare] setImage:[UIImage imageNamed:@"bomb.png"] forState:UIControlStateNormal];
            
        }else if ([cellsMatrix[iShare][jShare].type isEqualToString:@"oxigenio"]){
            [btnsMatrix[iShare][jShare] setImage:[UIImage imageNamed:@"oxygenLogo.png"] forState:UIControlStateNormal];
        }
        
        [self enableOneCell:iShare :jShare];
        //btnsMatrix[iShare][jShare].enabled = YES;
    }
}


- (void) changeFont {
    
    _lblOxigenio.font = [UIFont fontWithName:@"Skia-Regular_Black" size:15];
    _lblOxigenio.strokeColor = kStrokeColor2;
    _lblOxigenio.strokeSize = kStrokeSize3;
    
    _lblConta.font = [UIFont fontWithName:@"Skia-Regular_Black" size:30];
    _lblConta.strokeColor = kStrokeColor;
    _lblConta.strokeSize = kStrokeSize;
    
    _lblResposta.font = [UIFont fontWithName:@"Skia-Regular_Black" size:30];
    _lblResposta.strokeColor = kStrokeColor;
    _lblResposta.strokeSize = kStrokeSize;
    
}

// This function will change all components name to current language
- (void)internationalization {
    // label Oxigenio
    _lblOxigenio.text = NSLocalizedStringFromTableInBundle(@"OXYGEN", nil, [Util sharedInstance].localeBundle, nil);

}


-(void)fillBtnsMatrix{ //transforma array em matriz de celulas
    
    int i = 0, j = 0;
    for (UIButton *button in _cells) {
    
        [button addTarget:self action:selClickCell forControlEvents:UIControlEventTouchUpInside];
        
        btnsMatrix[i][j] = button;
        if(i < 4)
            i++;
        else{
            i = 0;
            j++;
        }
    }
}

-(void)fillCellsMatrix{
    
    //preenche matriz de celulas
    
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            
            iAtual = i;
            jAtual = j;
            
            cellsMatrix[i][j] = [[MatrixCell alloc] init];
            
            //preenche dados da celula
            [btnsMatrix[i][j] setImage:[UIImage imageNamed:@"addition.png"] forState:UIControlStateNormal];
            btnsMatrix[i][j].enabled = YES;
            btnsMatrix[i][j].alpha = 1;
            
            [cellsMatrix[i][j] setOperation:[self generateOperation]];
            [cellsMatrix[i][j] setExpression : [self randomizeExpression]];
            [cellsMatrix[i][j] setAnswer:resposta];
            [cellsMatrix[i][j] setType:tipoCell];
            [cellsMatrix[i][j] setIsAnswered:NO];

        }
    }
    
    iAtual = -1;
    jAtual = -1;
}

-(void)setKeyboardBtns{
    
    for (UIButton* button in _keyboard) {
        [button addTarget:self action:selClickKeyboard forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)btnCellClick:(UIButton*)sender{
    
    respostaAtual = @"";
    _lblAnswer.text = respostaAtual;
    
    int i, j = 0;
    for (i = 0; i < 5; i++) {
        for (j = 0; j < 4; j++) {
            if(sender == btnsMatrix[i][j])
                break;
        }
        if(sender == btnsMatrix[i][j])
            break;
    }
    //Os valores de i e j sao os correspondentes a celula na matriz
    iAtual = i;
    jAtual = j;
    iShare = i;
    jShare = j;
    
    _lblExpression.text = [cellsMatrix[i][j] expression];
    exp = _lblExpression.text;
    
    if ([cellsMatrix[iAtual][jAtual].type isEqualToString:@"timer"]) {
        [btnsMatrix[iAtual][jAtual] setImage:[UIImage imageNamed:@"clock.png"] forState:UIControlStateNormal];
        [self disableOtherCells];
        [self startTimer];
    }else if([cellsMatrix[iAtual][jAtual].type isEqualToString:@"bomba"]){
        [btnsMatrix[iAtual][jAtual] setImage:[UIImage imageNamed:@"bomb.png"] forState:UIControlStateNormal];
    }else if ([cellsMatrix[iAtual][jAtual].type isEqualToString:@"oxigenio"]){
        [btnsMatrix[iAtual][jAtual] setImage:[UIImage imageNamed:@"oxygenLogo.png"] forState:UIControlStateNormal];
    }
   
    
}

-(NSString*)randomizeExpression{
    
    NSString *expCell;
    
    if ([lvlD.extra isEqualToString:@"simples"]){
        expCell = [self randomizeSimples];
    }else if ([lvlD.extra isEqualToString:@"timerTodos"]){
        expCell = [self randomTodos];
    }else{
        expCell = [self extraRandom];
    }
    
    return expCell;
    
}

-(NSString*)extraRandom{
    
    NSString *expCell;
    int aux = arc4random()%7;
    if (aux == 0) {
        
        if([lvlD.extra isEqualToString:@"bomba"])
            expCell = [self randomizeBomba];
        else if([lvlD.extra isEqualToString:@"oxigenio"])
            expCell = [self randomizeOxigenio];
        else
            expCell = [self randomizeSimples];
    }else if (aux == 3 || aux == 2){
        if([lvlD.extra isEqualToString:@"timer"]){
            expCell = [self randomizeSimples];
            tipoCell = @"timer";
        }else{
            expCell = [self randomizeSimples];
        }
    }else{
        expCell = [self randomizeSimples];
    }
    return expCell;
}

-(NSString *)randomTodos{
    NSString *expCell;
    int aux = arc4random()%8;
    if (aux == 1) {
        expCell = [self randomizeBomba];
    }else if (aux == 2) {
        expCell = [self randomizeOxigenio];
    }else if (aux == 3 || aux == 4){
            expCell = [self randomizeSimples];
            tipoCell = @"timer";
    }else{
        expCell = [self randomizeSimples];
    }
//    [lvlD.operacao isEqualToString:@"+"];
//    lvlD = [[LevelDesign alloc] initLevelDesignWith:_lvl :@"+" :@"casaPlaneta1" :@"simples"];
    return expCell;
}

-(NSString*)randomizeSimples{
    
    int n1, n2;
    
    n1 = arc4random()%coef + 1;
    n2 = arc4random()%coef + 1;
    
    NSString *op;
    if ([cellsMatrix[iAtual][jAtual].operation isEqualToString:@"addition"]){
        op = @"+";
        cellsMatrix[iAtual][jAtual].answer = n1 + n2;
        
    }else if ([cellsMatrix[iAtual][jAtual].operation isEqualToString:@"subtraction"]){
        op = @"-";
        cellsMatrix[iAtual][jAtual].answer = n1 - n2;
    }else if ([cellsMatrix[iAtual][jAtual].operation isEqualToString:@"multiplication"]){
        op = @"x";
        cellsMatrix[iAtual][jAtual].answer = n1 * n2;
    }
    else if ([cellsMatrix[iAtual][jAtual].operation isEqualToString:@"division"]){
        op = @"/";
        
        while (n1%n2 != 0 || n1 == n2 || n2 <= 1) {
            n1 = arc4random()%coef*2 + 1;
            n2 = arc4random()%coef + 2;
        }
        cellsMatrix[iAtual][jAtual].answer = n1 / n2;
    }
    
    NSString *expCell = [[NSString alloc] initWithFormat:@"%d%@%d", n1,op, n2];
    
    resposta = [self solveExpression:expCell];
    tipoCell = @"simples";
    
    return expCell;
}

-(NSString*)randomizeBomba{
    
    int n1, n2, n3;
    
    n1 = arc4random()%coef + 2;
    n2 = arc4random()%coef + 1;
    n3 = arc4random()%coef + 2;
    
    NSString *expCell = [[NSString alloc] initWithFormat:@"%d-%d+%d", n1, n2, n3];
    
    resposta = n1 - n2 + n3;
    tipoCell = @"bomba";
    
    return expCell;
}

-(NSString*)randomizeOxigenio{
    
    int n1, n2, n3;
    
    n1 = arc4random()%coef;
    n2 = arc4random()%coef + 3;
    n3 = arc4random()%coef + 1;
    
    NSString *expCell = [[NSString alloc] initWithFormat:@"%dx%d-%d", n1, n2, n3];
    
    resposta = n1 * n2 - n3;
    tipoCell = @"oxigenio";
    
    return expCell;
}

-(void)eraseScreen{
    
    _lblAnswer.text = @"";
    _lblExpression.text = @"";
    _lblTime.text = @"";
    
    iAtual = -1;
    jAtual = -1;
    
    respostaAtual = @"";
}

-(void)btnKeyboardClick:(UIButton*)sender{
    
    int i = 0;
    for (UIButton* button in _keyboard) {
        if (button == sender) {
            break;
        }
        i++;
    }
    
    NSString *aux = [[NSString alloc] initWithFormat:@"%@%d", respostaAtual, i];
    
    
    if(iShare >= 0 && jShare >= 0){
        if (aux.length <= 3) {
            respostaAtual = aux;
            _lblAnswer.text = aux;
        }
    }
}

-(void)setLvlDesign{
    
    switch (_lvl) {
        case 1:
            lvlD = [[LevelDesign alloc] initLevelDesignWith:_lvl :@"+" :@"casaPlaneta1" :@"simples"];
            break;
            
        case 2:
            lvlD = [[LevelDesign alloc] initLevelDesignWith:_lvl :@"-" :@"dogPlaneta1" :@"bomba"];
            break;
            
        case 3:
            lvlD = [[LevelDesign alloc] initLevelDesignWith:_lvl :@"x" :@"carroPlaneta1" :@"oxigenio"];
            break;
            
        case 4:
            lvlD = [[LevelDesign alloc] initLevelDesignWith:_lvl :@"/" :@"geradorPlaneta1" :@"timer"];
            break;
            
        case 5:
            lvlD = [[LevelDesign alloc] initLevelDesignWith:_lvl :@"todos" :@"fabricaPlaneta1" :@"timerTodos"];
            break;
            
        default:
            break;
    }
    
}

-(int)solveExpression:(NSString*)expression{
    
    int result = 0;
    return result = cellsMatrix[iAtual][jAtual].answer;
}

-(void)checkCell: (int)i :(int)j{
    
    [cellsMatrix[i][j] setIsAnswered:YES];
    btnsMatrix[i][j].enabled = NO;
    btnsMatrix[i][j].alpha = 0.7;
    
}

-(void)setCellsImgs{
    
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            
            [btnsMatrix[i][j] setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", cellsMatrix[i][j].operation]] forState:UIControlStateNormal];
        }
    }
    
    
}

-(NSString*)generateOperation{
    
    NSArray *operations = @[@"addition", @"subtraction", @"multiplication", @"division"];
    
    if (_lvl == 5) {
        int index = arc4random()%4;
        return operations[index];
    }
    
    else
        return operations[_lvl - 1];
}

-(void)semOxigenio{
    
    [self callEndLevelController:0];
}

-(void)resetLevel{
    
    [self viewDidLoad];
    [self viewWillAppear:YES];
    
}


-(void)startTimer{
    
    // seta a variavel timerSet para true para indicar que o timer esta ativado, para ser utilizado no rascunho
    if (!timerSet) {
        timerSet = TRUE;
        _imgTimer.image = [UIImage imageNamed:@"timerON.png"];
        _lblTime.text = [NSString stringWithFormat:@"%ds", timeToResponse];
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(timePlaying)
                                                userInfo:nil
                                                 repeats: YES];
        [sounds playTimer:YES];
    }
    
}

- (void)timePlaying {
    
    timeToResponse--;
    
    _lblTime.text = [NSString stringWithFormat:@"%ds", timeToResponse];
    
    if (timeToResponse == 0) {
        [sounds playTimer:NO];
        
        oxigenio --;
        [self setImageOxygen];
        
        if (oxigenio == 0) {
            [self semOxigenio];
        }
        
        [self timerStop];
    }
}

-(void)timerStop{
    if(_timer)
    {
        [_timer invalidate];
        _timer = nil;
        timerSet = NO;
        [Util sharedInstance].trSetR = NO;
        timeToResponse = 15;
        [self eraseScreen];
        [self enableAllCells];
        [sounds playTimer:NO];
        _imgTimer.image = [UIImage imageNamed:@""];
        
    }
}

-(void)disableOtherCells{
    
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if (i != iAtual || j != jAtual) {
                btnsMatrix[i][j].enabled = NO;
            }
        }
    }
}

-(void)enableAllCells{
    
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++){
            if(!cellsMatrix[i][j].isAnswered)
                btnsMatrix[i][j].enabled = YES;
        }
    
}

-(void)enableOneCell:(int)icell :(int)jcell{
    btnsMatrix[icell][jcell].enabled = YES;
}

-(void)disableOneCell:(int)icell :(int)jcell{
    btnsMatrix[icell][jcell].enabled = NO;
}

-(BOOL)isAnswerRight{
    if (![cellsMatrix[iAtual][jAtual].type isEqualToString:@"timer"]) {
        if (cellsMatrix[iAtual][jAtual].answer == [respostaAtual intValue]) {
            return YES;
        }
        else
            return NO;
    }
    else if([_timer isValid]){
        if (cellsMatrix[iAtual][jAtual].answer == [respostaAtual intValue]) {
            return YES;
        }
        else
            return NO;
    }
    else
        return NO;
}

-(void)setImageOxygen{
    NSString *imgPath;
    imgPath = [NSString stringWithFormat:@"oxygen%d.png", oxigenio];
    _imgOxigenio.image = [UIImage imageNamed:imgPath];

}

-(void)setUserStars{
    User *user = [User sharedInstance];
    int stars;
    
    if(oxigenio == 4)
        stars = 3;
    else if(oxigenio >= 2)
        stars = 2;
    else
        stars = 1;
    
    [user setStars:stars InLevel:_lvl-1];
}

-(void)checkIfLevelIsOver{
    
    BOOL endFlag = YES;
    
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5;j++)
            if (!cellsMatrix[i][j].isAnswered)
                endFlag = NO;
    
    nAnswers++;
    
    if (nAnswers  >= 25){
        [self setUserStars];
        
        int stars;
        
        if(oxigenio == 4)
            stars = 3;
        else if(oxigenio >= 2)
            stars = 2;
        else
            stars = 1;
    
        LevelStatusStore *lvlSS = [LevelStatusStore sharedInstance];
        int atualStars = [lvlSS numberOfStarsInLevel:_lvl];
    
    [self callEndLevelController:stars];
    
        if (atualStars > stars)
            stars = atualStars;
        
    
        LevelStatus *lvlSAtual = [[LevelStatus alloc] initLevelStatusWith:_lvl :NO :YES :stars :1];
    
        [lvlSS updateStatus:lvlSAtual];
    
    if (_lvl < 5){
        int proximoStars = [lvlSS numberOfStarsInLevel:_lvl+1];
        BOOL proximoComplete = [lvlSS isComplete:1 :_lvl+1];
        LevelStatus *lvlSProximo = [[LevelStatus alloc] initLevelStatusWith:_lvl + 1:NO :proximoComplete :proximoStars :1];
        [lvlSS updateStatus:lvlSProximo];
    }
    
    }
    
}

-(void)callEndLevelController:(int)stars{
    
    NSString* endExpression;
    if (iAtual >= 0 && jAtual >= 0) {
         endExpression = [NSString stringWithFormat:@"%@=%d",_lblExpression.text, cellsMatrix[iAtual][jAtual].answer];
    }
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EndLevelViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"endLevel"];
    [vc setCompletedLevel:_lvl];
    [vc setStars:stars];
    [vc setExpression:endExpression];
    [vc setLvl:_lvl];
    
    [self presentViewController:vc animated:YES completion:nil];
}
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////ACOES BOTOES////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

- (IBAction)okBtnClick:(id)sender {
    
    if ((iAtual >= 0 && jAtual >=0) && ![respostaAtual isEqualToString:@""]) {
        
        if ([self isAnswerRight]) {
            
            [sounds playTimer:NO];
            
            [self enableAllCells];
            [self checkCell:iAtual :jAtual];
            
            if ([cellsMatrix[iAtual][jAtual].type isEqualToString:@"oxigenio"]) {
                
                if (oxigenio < 4){
                    oxigenio++;
                    [self setImageOxygen];
                }
            }
            
            if ([cellsMatrix[iAtual][jAtual].type isEqualToString: @"bomba"]) {
                
                if(iAtual + 1 < 5) {
                    [self checkCell :iAtual+1 :jAtual];
                    nAnswers++;
                }
                if(jAtual + 1 < 5) {
                    [self checkCell :iAtual :jAtual + 1];
                    nAnswers++;
                }
                if(iAtual - 1 >= 0) {
                    [self checkCell :iAtual-1 :jAtual];
                    nAnswers++;
                }
                if(jAtual - 1 >= 0) {
                    nAnswers++;
                    [self checkCell :iAtual :jAtual - 1];
                }
                
                [sounds playBombSound:YES];
            }else{
                [sounds playRightSound:YES];
            }
            
            [self eraseScreen];
            [self timerStop];
            
            [self checkIfLevelIsOver];
        }
        else{
           
            [sounds playWrongSound:YES];
            respostaAtual = @"";
            _lblAnswer.text = @"";
            
                oxigenio--;
                [self setImageOxygen];
                
                if (oxigenio == 0)
                    [self semOxigenio];

        }
    }
}

- (IBAction)eraseBtnCLick:(id)sender {
    
    respostaAtual = @"";
    _lblAnswer.text = respostaAtual;
}

- (IBAction)btnRascunho:(id)sender {

    SketchViewController *rvc = [self.storyboard instantiateViewControllerWithIdentifier:@"rascunho"];

    rvc.expressao = exp;
    
    rvc.trSet = timerSet;
    rvc.tempoFalta = timeToResponse;
    
    [self presentViewController:rvc animated:YES completion:nil];
    
}

- (IBAction)btnReset:(id)sender {
    
    NSString *resetLevel = NSLocalizedStringFromTableInBundle(@"RESET_LEVEL", nil, [Util sharedInstance].localeBundle, nil);;
    NSString *msgAlert = NSLocalizedStringFromTableInBundle(@"MSG_RESET_LEVEL", nil, [Util sharedInstance].localeBundle, nil);;
    NSString *cancel = NSLocalizedStringFromTableInBundle(@"CANCEL", nil, [Util sharedInstance].localeBundle, nil);;
    
    UIAlertController *alerta = [UIAlertController alertControllerWithTitle:resetLevel message:msgAlert preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
        
        [self timerStop];
        [sounds stopAllSoundsEffects];
        [self resetLevel];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
        
    }];
    
    [alerta addAction:cancelAction];
    [alerta addAction:okAction];
    
    [self presentViewController:alerta animated:YES completion:nil];
}



- (IBAction)btnVoltar:(id)sender {
    [self timerStop];
    [sounds stopAllSoundsEffects];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnSinalAction:(id)sender {
    
    if (![respostaAtual isEqualToString:@""]) {
        int answer = [respostaAtual intValue];
        answer = -1*answer;
        respostaAtual = [NSString stringWithFormat:@"%d", answer];
        _lblAnswer.text = respostaAtual;
    }
}

- (IBAction)btnOptions:(id)sender {
    [self timerStop];
    [sounds stopAllSoundsEffects];
    ConfigurationViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"options"];
    
    [self presentViewController:cvc animated:YES completion:nil];
}

@end
