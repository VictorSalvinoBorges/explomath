//
//  Util.m
//  Explomath
//
//  Created by Cristian Simioni Milani on 24/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "Util.h"

@implementation Util

+(Util *)sharedInstance{
    static Util *_sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[super alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *language ;
        NSString *temp = [defaults stringForKey:@"LanguageSelected"] ;
        if (temp != nil) {
            language = [defaults stringForKey:@"LanguageSelected"];
        }else{
            [defaults setObject:@"pt" forKey:@"LanguageSelected"];
            language = @"pt";
        }
        
        NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
        if (path) {
            _localeBundle = [NSBundle bundleWithPath:path];
        }
        else {
            _localeBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"] ];
        }
        
        // variaveis que serao usadas na view do jogo
        _trSetR = NO;
        _timerValueR = 0;
        _expRasc = [[NSString alloc]init];
        _icell = 0;
        _jcell = 0;
    }
    return self;
}

@end
