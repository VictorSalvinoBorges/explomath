//
//  RascunhoViewController.h
//  Explomath
//
//  Created by Bruna Matos on 20/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SketchViewController : UIViewController{
    CGPoint lastPoint;
    CGPoint moveBackTo;
    CGPoint currentPoint;
    CGPoint location;
    NSDate *lastClick;
    BOOL mouseSwiped;
    UIImageView *drawImage;
    UIImageView *frontImage;
}


@property NSTimer* timer;
@property BOOL trSet;

// variaveis que vem da view do jogo
@property NSString *expressao;
@property int tempoFalta;


@end
