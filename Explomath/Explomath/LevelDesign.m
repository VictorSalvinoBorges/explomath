//
//  LevelDesign.m
//  Explomath
//
//  Created by Victor Salvino Borges on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "LevelDesign.h"

@implementation LevelDesign

- (id)initLevelDesignWith:(int)level :(NSString*)operacao :(NSString *)achievement :(NSString*)extra{
    
    self = [super init];
    if(self){
        _level = level;
        _operacao = operacao;
        _achievement = achievement;
        _extra = extra;
    }
    return self;
}

@end
