//
//  AjudaViewController.m
//  Explomath
//
//  Created by Bruna Matos on 30/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "AjudaViewController.h"
#import "Constants.h"
#import "THLabel.h"
#import "Util.h"

@interface AjudaViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnVoltar;
@property (weak, nonatomic) IBOutlet UIButton *btnFrente;
@property (weak, nonatomic) IBOutlet THLabel *lblAjuda;
@property (weak, nonatomic) IBOutlet THLabel *lblAjuda1;
@property (weak, nonatomic) IBOutlet THLabel *lblAjuda2;
@property (weak, nonatomic) IBOutlet THLabel *lblAjuda3;

@property (weak, nonatomic) IBOutlet UIImageView *imgAjuda1;
@property (weak, nonatomic) IBOutlet UIImageView *imgAjuda2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constImage1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constImage2;
@end

@implementation AjudaViewController{
    int page;
    NSString *msg1;
    NSString *msg2;
    NSString *msg3;
    NSString *img1;
    NSString *img2;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self changeFont];
    [self internationalization];
    page = 1;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [self setPageContent];
    [self internationalization];
}



-(void)setPageContent{
    if (page ==1) {
        msg1 = @"MSG1_HELP1";
        msg2 = @"MSG2_HELP1";
        msg3 = @"";
        _imgAjuda1.image = [UIImage imageNamed:@"ajuda1.png"];
        _imgAjuda2.image = [UIImage imageNamed:@"ajuda2.png"];
        _constImage1.constant = 143;
        _constImage2.constant = 143;
        
    }else if (page == 2){
        msg1 = @"MSG1_HELP2";
        msg2 = @"MSG2_HELP_2";
        msg3 = @"";
        _imgAjuda1.image = [UIImage imageNamed:@"ajuda3.png"];
        _imgAjuda2.image = [UIImage imageNamed:@"ajuda4.png"];
        _constImage1.constant = 143;
        _constImage2.constant = 143;
    }else{
        msg1 = @"MSG1_HEP3";
        msg2 = @"MSG2_HEP3";
        msg3 = @"MSG3_HEP3";
        _imgAjuda1.image = [UIImage imageNamed:@"ajuda5.png"];
        _imgAjuda2.image = [UIImage imageNamed:@"ajuda6.png"];
        _constImage1.constant = 78;
        _constImage2.constant = 78;
    }
}

- (void)internationalization {
    _lblAjuda.text = NSLocalizedStringFromTableInBundle(@"HELP", nil, [Util sharedInstance].localeBundle, nil);
    _lblAjuda1.text = NSLocalizedStringFromTableInBundle(msg1, nil, [Util sharedInstance].localeBundle, nil);
    _lblAjuda2.text = NSLocalizedStringFromTableInBundle(msg2, nil, [Util sharedInstance].localeBundle, nil);
    _lblAjuda3.text = NSLocalizedStringFromTableInBundle(msg3, nil, [Util sharedInstance].localeBundle, nil);
}

- (void) changeFont {
    

    _lblAjuda.font = [UIFont fontWithName:@"Skia-Regular_Black" size:45];
    _lblAjuda.strokeColor = kStrokeColor;
    _lblAjuda.strokeSize = kStrokeSize;
    
    _lblAjuda1.font = [UIFont fontWithName:@"Skia-Regular_Black" size:12];
    _lblAjuda1.strokeColor = kStrokeColor;
    _lblAjuda1.strokeSize = kStrokeSize;
    
    _lblAjuda2.font = [UIFont fontWithName:@"Skia-Regular_Black" size:12];
    _lblAjuda2.strokeColor = kStrokeColor;
    _lblAjuda2.strokeSize = kStrokeSize;
    
    _lblAjuda3.font = [UIFont fontWithName:@"Skia-Regular_Black" size:12];
    _lblAjuda3.strokeColor = kStrokeColor;
    _lblAjuda3.strokeSize = kStrokeSize;

}


- (IBAction)btnVoltarAction:(id)sender {
    page--;
    [self setPageContent];
    [self internationalization];
    if (page == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    _btnFrente.hidden = NO;
    
}

- (IBAction)btnFrenteAction:(id)sender {
    page = page + 1;
    [self setPageContent];
    [self internationalization];
    if (page == 3) {
        _btnFrente.hidden = YES;
    }
}

@end
