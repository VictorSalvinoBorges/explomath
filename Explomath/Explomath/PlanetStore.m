//
//  PlanetStore.m
//  Explomath
//
//  Created by Victor Salvino Borges on 24/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "PlanetStore.h"
#import "FileManager.h"
#import "Planet.h"

#define PLANET_FILE_JSON @"Planet.json"
NSString *const planetFile = PLANET_FILE_JSON;

@implementation PlanetStore{
    NSMutableArray *_PlanetsArray;
}

+ (PlanetStore *)sharedInstance{
    
    static PlanetStore *_sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[super alloc] init];
    });
    
    return _sharedInstance;
}

- (id) init {
    self = [super init];
    if (self) {
        _PlanetsArray = [[NSMutableArray alloc] init];
        [self readPlanets];
    }
    return self;
}

-(void)readPlanets{
    NSError *error;
    NSData *jsonData = [NSData dataWithContentsOfFile:[FileManager getPathFromDocuments:planetFile]];
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    if (!data) {
        NSLog(@"Error loading level status: %@", error);
    }
    
    NSArray *jLevelStatus = [data objectForKey:@"planet"];
    
    for (NSDictionary *l in jLevelStatus) {
        NSString *name = [l objectForKey:@"name"];
        int number = [[l objectForKey:@"number"] intValue];
        NSString *imgplanet = [l objectForKey:@"image"];
        BOOL blocked = [[l objectForKey:@"blocked"] intValue] == 1 ? YES : NO;
        
        Planet *planetTemp = [[Planet alloc] initPlanetNameWith:name Number:number StrImagPlanet:imgplanet Blocked: blocked];
        
        [_PlanetsArray addObject:planetTemp];
    }

}

- (void)updateBlocked:(Planet *) plplanet {
    NSError *error;
    NSData *jsonData = [NSData dataWithContentsOfFile:[FileManager getPathFromDocuments:levelStatusFile]];
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    if (!data) {
        NSLog(@"Error loading planet status: %@", error);
    }
    
    NSMutableArray *jLevelStatus = [[data objectForKey:@"planet"] mutableCopy];
    
    int i = 0;
    for (NSMutableDictionary *l in jLevelStatus) {
        if ([[l objectForKey:@"number"] intValue] == plplanet.numberPlanet) {
            if (!plplanet.blocked) {
                [l setObject:@"0" forKey:@"blocked"];
            }
            
            [_PlanetsArray removeObjectAtIndex:i];
            [_PlanetsArray insertObject:plplanet atIndex:i];
            break;
        }
        i++;
    }
    
    NSDictionary *levelsStatusDictionary = [NSDictionary dictionaryWithObject:jLevelStatus forKey:@"status"];
    
    jsonData = [NSJSONSerialization dataWithJSONObject:levelsStatusDictionary options:0 error:&error];
    [jsonData writeToFile:[FileManager getPathFromDocuments:levelStatusFile] atomically:YES];
}

-(Planet *)getPlanet:(int)index{
    return [_PlanetsArray objectAtIndex:index];
}

-(int)count{
    return _PlanetsArray.count;
}

@end
