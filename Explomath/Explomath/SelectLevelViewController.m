//
//  SelecioneNivelViewController.m
//  Explomath
//
//  Created by Bruna Matos on 22/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "SelectLevelViewController.h"
#import "LevelViewController.h"
#import "THLabel.h"
#import "Util.h"
#import "Constants.h"
#import "ConfigurationViewController.h"
#import "LevelStatusStore.h"

@interface SelectLevelViewController (){
    int selectedPlanet;
    int selectedLevel;
    
}

// Labels
@property (weak, nonatomic) IBOutlet UILabel *lblSelectLevel;
// Buttons
@property (weak, nonatomic) IBOutlet UIButton *btnExplore;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *levelButtons;
// Images
@property (weak, nonatomic) IBOutlet UIImageView *imgStars;
// Stroke
@property (weak, nonatomic) IBOutlet THLabel *lblCSelecioneNivel;

@end

@implementation SelectLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self changeFont];
    
    selectedPlanet = 1;
}

-(void)viewWillAppear:(BOOL)animated{
    _btnExplore.enabled = NO;
    [self internationalization];
    //[self setEnabledLevels];
    [self changeImages:-1];
    [self setEnabledButtons];
    _imgStars.hidden = YES;
}

- (void) changeFont {
    // Select Level
    _lblSelectLevel.font = [UIFont fontWithName:@"Skia-Regular_Black" size:30];
    _lblCSelecioneNivel.strokeColor = kStrokeColor;
    _lblCSelecioneNivel.strokeSize = kStrokeSize;
}

// This function will change all components name to current language
- (void)internationalization {
    // Label Select a Level
    _lblSelectLevel.text = NSLocalizedStringFromTableInBundle(@"SELECT_A_LEVEL", nil, [Util sharedInstance].localeBundle, nil);
    
    // Explore Button
    NSString *exploreImageName = NSLocalizedStringFromTableInBundle(@"EXPLORE_BTN", nil, [Util sharedInstance].localeBundle, nil);
    [_btnExplore setImage:[UIImage imageNamed:exploreImageName] forState:UIControlStateNormal];
}

- (IBAction)btnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnFase1:(id)sender {
    
    selectedLevel = 1;
    [self changeImages:selectedLevel];
    [self changeStarsImg:selectedLevel];
    _btnExplore.enabled = YES;
}

- (IBAction)btnFase2:(id)sender {
    
    selectedLevel = 2;
    [self changeImages:selectedLevel];
    [self changeStarsImg:selectedLevel];
    _btnExplore.enabled = YES;
}

- (IBAction)btnFase3:(id)sender {
    
    selectedLevel = 3;
    [self changeImages:selectedLevel];
    [self changeStarsImg:selectedLevel];
    _btnExplore.enabled = YES;
}

- (IBAction)btnFase4:(id)sender {
    
    selectedLevel = 4;
    [self changeImages:selectedLevel];
    [self changeStarsImg:selectedLevel];
    _btnExplore.enabled = YES;
}

- (IBAction)btnFase5:(id)sender {
    
    selectedLevel = 5;
    [self changeImages:selectedLevel];
    [self changeStarsImg:selectedLevel];
    _btnExplore.enabled = YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    LevelViewController *levelViewController = [segue destinationViewController];
    [levelViewController setLvl:selectedLevel];
}

- (IBAction)btnOptions:(id)sender {
    ConfigurationViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"options"];
    
    [self presentViewController:cvc animated:YES completion:nil];
}

- (void)changeImages:(int) level {
    
    if (level != -1) {
        NSString *highlight = [NSString stringWithFormat:@"0%d_highlight", level];
        [_levelButtons[level - 1] setImage:[UIImage imageNamed:highlight] forState:UIControlStateNormal];
    }
   
    for (int i = 0; i < 5; i++) {
        NSString *active = [NSString stringWithFormat:@"0%d_active", i+1];;
        NSString *inactive = [NSString stringWithFormat:@"0%d_inactive", i+1];;
        
        if (i != level - 1) {
            // Verify if the level is blocked
            if ([[LevelStatusStore sharedInstance] isBlocked:selectedPlanet :i+1]) {
                [_levelButtons[i] setImage:[UIImage imageNamed:inactive] forState:UIControlStateNormal];
            } else {
                [_levelButtons[i] setImage:[UIImage imageNamed:active] forState:UIControlStateNormal];
            }
        }
    }
}

- (void) setEnabledButtons {
    selectedPlanet = 1;
    for (int i = 0; i < 5; i++) {
        // Disable the level button if it is not complete
        if ([[LevelStatusStore sharedInstance] isBlocked:selectedPlanet :i+1]) {
                [[_levelButtons objectAtIndex:i] setEnabled:NO];
        }
        else{
            [[_levelButtons objectAtIndex:i] setEnabled:YES];
        }
    }
}

-(void)changeStarsImg:(int)lvl{
    
    int nStars = [[LevelStatusStore sharedInstance] numberOfStarsInLevel:lvl];
    NSString *imgName = [NSString stringWithFormat:@"estrelas%d", nStars];
    
    [_imgStars setImage:[UIImage imageNamed:imgName]];
    _imgStars.hidden = NO;
}


@end
