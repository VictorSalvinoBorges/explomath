//
//  Sound.m
//  Explomath
//
//  Created by Victor Salvino Borges on 23/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "Sound.h"
#import <AVFoundation/AVFoundation.h>

@implementation Sound

-(id)initSoundsBackgroundWith:(NSURL *)urlBackgroundSnd UrlRight:(NSURL *)urlRightSnd UrlWrong:(NSURL *)urlWrongSnd UrlBomb:(NSURL *)urlBombSnd UrlTimer:(NSURL *)urlTimerSnd{
    
    self = [super init];
    if (self) {
        _sndBackground = [[AVAudioPlayer alloc] initWithContentsOfURL:urlBackgroundSnd error:nil];
        _sndRight = [[AVAudioPlayer alloc] initWithContentsOfURL:urlRightSnd error:nil];
        _sndWrong = [[AVAudioPlayer alloc] initWithContentsOfURL:urlWrongSnd error:nil];
        _sndBomb = [[AVAudioPlayer alloc] initWithContentsOfURL:urlBombSnd error:nil];
        _sndTimer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlTimerSnd error:nil];
    }
    return self;
}


@end
