//
//  LevelDesign.h
//  Explomath
//
//  Created by Victor Salvino Borges on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LevelDesign : NSObject

@property int level;
@property NSString* operacao;
@property NSString* achievement;
@property NSString* extra;

- (id) initLevelDesignWith:(int)level :(NSString*)operacao :(NSString*)achievement :(NSString*)extra;

@end
