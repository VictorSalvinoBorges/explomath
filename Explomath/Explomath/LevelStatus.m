//
//  Status.m
//  Explomath
//
//  Created by Cristian Simioni Milani on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "LevelStatus.h"

@implementation LevelStatus

- (id) initLevelStatusWith:(int)level :(BOOL)andBlocked :(BOOL)andComplete :(int)andStars :(int)andPlanet {
    self = [super init];
    if (self) {
        _level = level;
        _blocked = andBlocked;
        _complete = andComplete;
        _stars = andStars;
        _planet = andPlanet;
    }
    return self;
}

@end
