//
//  Planet.h
//  Explomath
//
//  Created by Victor Salvino Borges on 24/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Planet : NSObject

@property NSString *name;
@property int numberPlanet;
@property NSString *imagPlanet;
@property BOOL blocked;

- (id) initPlanetNameWith:(NSString *)name Number:(int)numberPlanet StrImagPlanet:(NSString *)imagPlanet Blocked:(BOOL)blocked;

@end
