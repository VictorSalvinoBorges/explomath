//
//  LevelViewController.h
//  Explomath
//
//  Created by Matheus Cassol on 20/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LevelViewController : UIViewController

@property int lvl;
@property NSTimer* timer;

-(void)resetLevel;

@end
