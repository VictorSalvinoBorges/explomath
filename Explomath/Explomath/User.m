//
//  User.m
//  Explomath
//
//  Created by Cristian Simioni Milani on 21/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "User.h"

@implementation User

+(User *)sharedInstance{
    static User *_sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[super alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        for (int i = 0; i < 5; i++) {
            stars[i] = 0;
        }
    }
    return self;
}

-(void)setStars:(int)nStars InLevel:(int)lvl{
    stars[lvl] = nStars;
}

-(int *)getStars{
    return stars;
}

@end
