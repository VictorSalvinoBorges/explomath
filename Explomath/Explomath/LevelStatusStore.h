//
//  LevelStatusStore.h
//  Explomath
//
//  Created by Cristian Simioni Milani on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const levelStatusFile;

@class LevelStatus;

@interface LevelStatusStore : NSObject

+ (LevelStatusStore*) sharedInstance;

- (void) readStatus;

- (void) updateStatus:(LevelStatus*) level;

- (int) numberOfLevelsClosedFor:(int) planet;

- (int) lastLevel;

- (int) lastPlanet;

- (BOOL) isBlocked:(int) planet :(int) level;

-(int)numberOfStarsInLevel:(int)lvl;

-(BOOL)isComplete:(int)planet :(int) level;

@end
