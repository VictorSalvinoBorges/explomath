//
//  LevelDesignStore.h
//  Explomath
//
//  Created by Victor Salvino Borges on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const levelDesignFile;

@class LevelDesign;

@interface LevelDesignStore : NSObject

+ (LevelDesignStore*) sharedInstance;

- (void) readLevel;



@end
