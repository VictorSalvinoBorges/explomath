//
//  EndLevelViewController.h
//  Explomath
//
//  Created by Matheus Cassol on 27/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EndLevelViewController : UIViewController

@property int completedLevel;
@property int stars;
@property NSString *expression;
@property int lvl;

@end
