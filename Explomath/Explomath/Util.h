//
//  Util.h
//  Explomath
//
//  Created by Cristian Simioni Milani on 24/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Util : NSObject

@property NSBundle *localeBundle;
// propriedades que virao da view rascunho
@property BOOL trSetR;        // se o timer esta ativado no rascunho
@property int timerValueR;    // o valor do timer na view do rascunho
@property NSString *expRasc;  // expressao que estava no rascunho
@property int icell,jcell;

+(Util *)sharedInstance;

@end