//
//  SelecionePlanetaViewController.m
//  Explomath
//
//  Created by Bruna Matos on 21/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "SelectPlanetViewController.h"
#import "LevelStatusStore.h"
#import "THLabel.h"
#import "Constants.h"
#import "Util.h"
#import "PlanetStore.m"
#import "ConfigurationViewController.h"

@interface SelectPlanetViewController (){
    PlanetStore *aryPlanets;
}

@property (weak, nonatomic) IBOutlet UILabel *lblSelecionePlaneta;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaneta;
@property (weak, nonatomic) IBOutlet UILabel *lblNiveisCompletos;
// Variable
@property (weak, nonatomic) IBOutlet UILabel *lblNumeroPlaneta;
@property (weak, nonatomic) IBOutlet UILabel *lblNumeroNiveisCompletos;
@property (weak, nonatomic) IBOutlet UIButton *imgPlanet;
@property int currentPlanet;
@property (weak, nonatomic) IBOutlet UILabel *lblPlanetName;
@property (weak, nonatomic) IBOutlet UIButton *btnLeftChange;
@property (weak, nonatomic) IBOutlet UIButton *btnRightChange;


// Contorno
@property (weak, nonatomic) IBOutlet THLabel *lblCSelPlaneta;
@property (weak, nonatomic) IBOutlet THLabel *lblCNomePlan;
@property (weak, nonatomic) IBOutlet THLabel *lblCPlaneta;
@property (weak, nonatomic) IBOutlet THLabel *lblCPlanNumero;
@property (weak, nonatomic) IBOutlet THLabel *lblCNiveisCom;
@property (weak, nonatomic) IBOutlet THLabel *lblCNiveisNumero;


@end

@implementation SelectPlanetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self internationalization];
    
    [self changeFont];
    
    _currentPlanet = 0;
    
    
    //Cristian, coloquei esse código aqui só para testar se meu código estava funcionando
    [FileManager moveFromBundleToDocuments:PLANET_FILE_JSON];
    aryPlanets = [PlanetStore sharedInstance];
    
    [self updateScreen];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self internationalization];
    [self updateScreen];
}

- (void)viewDidAppear:(BOOL)animated {
    [self internationalization];
    [self updateScreen];
}

- (void) changeFont {
    
    // Select a planet
    _lblSelecionePlaneta.font = [UIFont fontWithName:@"Skia-Regular_Black" size:30];
    _lblCSelPlaneta.strokeColor = kStrokeColor;
    _lblCSelPlaneta.strokeSize = kStrokeSize;

    // name of planet
    _lblPlanetName.font = [UIFont fontWithName:@"Skia-Regular_Black" size:30];
    _lblCNomePlan.strokeColor = kStrokeColor;
    _lblCNomePlan.strokeSize = kStrokeSize;
    
    // Planet
    _lblPlaneta.font = [UIFont fontWithName:@"Skia-Regular_Black" size:25];
    _lblCPlaneta.strokeColor = kStrokeColor;
    _lblCPlaneta.strokeSize = kStrokeSize;
    
    // Complete levels
    _lblNiveisCompletos.font = [UIFont fontWithName:@"Skia-Regular_Black" size:25];
    _lblCPlanNumero.strokeColor = kStrokeColor;
    _lblCPlanNumero.strokeSize = kStrokeSize2;
    
    // Planet number
    _lblNumeroPlaneta.font = [UIFont fontWithName:@"Skia-Regular_Black" size:60];
    _lblCNiveisCom.strokeColor = kStrokeColor;
    _lblCNiveisCom.strokeSize = kStrokeSize;
    
    // Complete levels number
    _lblNumeroNiveisCompletos.font = [UIFont fontWithName:@"Skia-Regular_Black" size:60];
    _lblCNiveisNumero.strokeColor = kStrokeColor;
    _lblCNiveisNumero.strokeSize = kStrokeSize2;
}

- (void)internationalization {
    _lblSelecionePlaneta.text = NSLocalizedStringFromTableInBundle(@"SELECT_A_PLANET", nil, [Util sharedInstance].localeBundle, nil);
    _lblPlaneta.text = NSLocalizedStringFromTableInBundle(@"PLANET", nil, [Util sharedInstance].localeBundle, nil);
    _lblNiveisCompletos.text = NSLocalizedStringFromTableInBundle(@"COMPLETED", nil, [Util sharedInstance].localeBundle, nil);
}

- (IBAction)btnOpcoes:(id)sender {
    ConfigurationViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"options"];
    
    [self presentViewController:cvc animated:YES completion:nil];
    
}

- (IBAction)btnVoltar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnLeft:(id)sender {
    if (_currentPlanet > 0) {
        _currentPlanet--;
        
        [self updateScreen];
        
    }
}

- (IBAction)btnRight:(id)sender {
    int numberOfPlanets = [aryPlanets count];
    
    if (_currentPlanet < numberOfPlanets - 1) {
        _currentPlanet ++;
        
        [self updateScreen];
    }
    
}

-(void)updateScreen{
    Planet *temPlanet = [aryPlanets getPlanet:_currentPlanet];
    
    _lblNumeroPlaneta.text = [NSString stringWithFormat:@"%d", temPlanet.numberPlanet];
    
    _lblPlanetName.text = temPlanet.name;
    NSString *imgPath;
    
    if (temPlanet.blocked == NO) {
        imgPath = [NSString stringWithFormat:@"planet0%dON.png", temPlanet.numberPlanet];
        _imgPlanet.enabled = YES;
    }else{
        imgPath = [NSString stringWithFormat:@"planet0%dOFF.png", temPlanet.numberPlanet];
        _imgPlanet.enabled = NO;
    }
    [_imgPlanet setImage:[UIImage imageNamed:imgPath] forState:UIControlStateNormal];
    
    _lblNumeroNiveisCompletos.text = [[NSString stringWithFormat:@"%d", [[LevelStatusStore sharedInstance] numberOfLevelsClosedFor:_currentPlanet+1]] stringByAppendingString:@"/5"];
    
    if (_currentPlanet > 0) {
        _btnLeftChange.enabled = YES;
    }else {
        // Disable left button
        _btnLeftChange.enabled = NO;
    }
    
    if (_currentPlanet < [aryPlanets count]-1 ) {
        _btnRightChange.enabled =YES;
    }else {
        // Disable right button
        _btnRightChange.enabled = NO;
    }
}

@end
