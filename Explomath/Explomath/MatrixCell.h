//
//  MatrixCell.h
//  Batalha Naval
//
//  Created by Matheus Cassol on 17/04/15.
//  Copyright (c) 2015 Matheus Cassol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MatrixCell : NSObject

@property int answer;               //resposta correta da celula
@property NSString *expression;     //expressao  da celula
@property NSString *type;           //tipo da celula (normal, bomba, tempo, oxigenio)
@property BOOL isAnswered;          //se a celula ja foi respondida ou nao
@property NSString *image;          // Cell image
@property NSString *operation; //operacao da celula

- (id) initWithAnswer:(int) answer :(NSString *)andExpression :(NSString *)type;

@end
