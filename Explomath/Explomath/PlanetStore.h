//
//  PlanetStore.h
//  Explomath
//
//  Created by Victor Salvino Borges on 24/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const planetFile;

@class Planet;

@interface PlanetStore : NSObject

+ (PlanetStore*) sharedInstance;

- (void) readPlanets;

- (void)updateBlocked:(Planet*)Planeta;

-(Planet *)getPlanet:(int)index;

@end
