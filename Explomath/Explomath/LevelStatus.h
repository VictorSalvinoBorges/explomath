//
//  Status.h
//  Explomath
//
//  Created by Cristian Simioni Milani on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LevelStatus : NSObject

@property int level;
@property BOOL blocked;
@property BOOL complete;
@property int stars;
@property int planet;

- (id) initLevelStatusWith:(int)level :(BOOL)andBlocked :(BOOL)andComplete :(int)andStars :(int)andPlanet;

@end
