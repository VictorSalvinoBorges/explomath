//
//  telaInicialViewController.m
//  Explomath
//
//  Created by Bruna Matos on 20/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "YourPlanetViewController.h"
#import "LevelStatusStore.h"
#import "LevelStatus.h"
#import "THLabel.h"
#import "Constants.h"
#import "Util.h"

@interface YourPlanetViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imgMyPlanet;
@property (weak, nonatomic) IBOutlet UILabel *lblYourPlanet;
@property (weak, nonatomic) IBOutlet UIButton *btnOptions;
@property (weak, nonatomic) IBOutlet UIButton *btnExplore;
// Stroke
@property (weak, nonatomic) IBOutlet THLabel *lblSeuPlaneta;

@end

@implementation YourPlanetViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self changeFont];
    [self internationalization];
    [self getCurrentState];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self internationalization];
    [self getCurrentState];
}

// This function will change all components name to current language
- (void)internationalization {
    // Your Planet Label
    _lblYourPlanet.text = NSLocalizedStringFromTableInBundle(@"YOUR_PLANET", nil, [Util sharedInstance].localeBundle, nil);
    
    // Option Button
    NSString *optionsImageName = NSLocalizedStringFromTableInBundle(@"OPTIONS_BTN", nil, [Util sharedInstance].localeBundle, nil);
    [_btnOptions setImage:[UIImage imageNamed:optionsImageName] forState:UIControlStateNormal];
    
    // Explore Button
    NSString *exploreImageName = NSLocalizedStringFromTableInBundle(@"EXPLORE_BTN", nil, [Util sharedInstance].localeBundle, nil);
    [_btnExplore setImage:[UIImage imageNamed:exploreImageName] forState:UIControlStateNormal];
}

- (void) changeFont {
    
    _lblYourPlanet.font = [UIFont fontWithName:@"Skia-Regular_Black" size:40];
    _lblSeuPlaneta.strokeColor = kStrokeColor;
    _lblSeuPlaneta.strokeSize = kStrokeSize;
    
}

- (void) getCurrentState {
    // Get current state completed
    int lastPlanet = [[LevelStatusStore sharedInstance] lastPlanet];
    int lastLevel = [[LevelStatusStore sharedInstance] lastLevel];
    
    // Change planet image to current state
    NSString *imgYourPlanet = [NSString stringWithFormat:@"yourPlanet_%d_%d.png", lastPlanet, lastLevel];
    _imgMyPlanet.image = [UIImage imageNamed:imgYourPlanet];
}

@end

