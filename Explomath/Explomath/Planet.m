//
//  Planet.m
//  Explomath
//
//  Created by Victor Salvino Borges on 24/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "Planet.h"

@implementation Planet

-(id)initPlanetNameWith:(NSString *)name Number:(int)numberPlanet StrImagPlanet:(NSString *)imagPlanet Blocked:(BOOL)blocked{
    self = [super init];
    if (self) {
        _name = name;
        _numberPlanet = numberPlanet;
        _imagPlanet = imagPlanet;
        _blocked = blocked;
    }
    return self;
}

@end
