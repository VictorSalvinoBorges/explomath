//
//  SobreViewController.m
//  Explomath
//
//  Created by Bruna Matos on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "AboutViewController.h"
#import "THLabel.h"
#import "Constants.h"
#import "Util.h"

@interface AboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblDevelopedBy;
@property (weak, nonatomic) IBOutlet UILabel *lblAuthors;

@property (weak, nonatomic) IBOutlet UILabel *lblMusicos;
@property (weak, nonatomic) IBOutlet UILabel *lblMusicBy;


@property (weak, nonatomic) IBOutlet UILabel *lblArtDesigners;
@property (weak, nonatomic) IBOutlet UILabel *lblArtDesBy;

// Contornos
@property (weak, nonatomic) IBOutlet THLabel *lblCADesBy;
@property (weak, nonatomic) IBOutlet THLabel *lblCMusicBy;
@property (weak, nonatomic) IBOutlet THLabel *lblCDevBy;



@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self changeFont];
    [self internationalization];
}

-(void)viewWillAppear:(BOOL)animated{
    [self internationalization];
}

// This function will change all components name to current language
- (void)internationalization {
    _lblDevelopedBy.text = NSLocalizedStringFromTableInBundle(@"DEVELOPED_BY", nil, [Util sharedInstance].localeBundle, nil);
    _lblMusicBy.text = NSLocalizedStringFromTableInBundle(@"MUSIC_BY", nil, [Util sharedInstance].localeBundle, nil);
    _lblArtDesBy.text = NSLocalizedStringFromTableInBundle(@"ART", nil, [Util sharedInstance].localeBundle, nil);
}

- (void) changeFont {
    
    // Developed by Label
    _lblDevelopedBy.font = [UIFont fontWithName:@"Skia-Regular_Black" size:20];
    _lblCDevBy.strokeColor = kStrokeColor;
    _lblCDevBy.strokeSize = kStrokeSize;
    
    // Authors
    //_lblAuthors.font = [UIFont fontWithName:@"Skia-Regular_Black" size:20];
    
    //_lblMusicos.font = [UIFont fontWithName:@"Skia-Regular_Black" size:20];
    _lblMusicBy.font = [UIFont fontWithName:@"Skia-Regular_Black" size:20];
    _lblCMusicBy.strokeColor = kStrokeColor;
    _lblCMusicBy.strokeSize = kStrokeSize;
    
    //_lblArtDesigners.font = [UIFont fontWithName:@"Skia-Regular_Black" size:20];
    _lblArtDesBy.font = [UIFont fontWithName:@"Skia-Regular_Black" size:20];
    _lblCADesBy.strokeColor = kStrokeColor;
    _lblCADesBy.strokeSize = kStrokeSize;
}

- (IBAction)btnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
