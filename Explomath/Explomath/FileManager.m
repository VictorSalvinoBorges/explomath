//
//  FileManager.m
//  Explomath
//
//  Created by Cristian Simioni Milani on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import "FileManager.h"

@implementation FileManager

+ (void)moveFromBundleToDocuments:(NSString *)file {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectoryPath = [paths objectAtIndex:0];
    NSString *destinationPath = [documentDirectoryPath stringByAppendingPathComponent:file];
    
    NSLog(@"File: %@", destinationPath);
    
    if([fileManager fileExistsAtPath:destinationPath])
        return;
    
    NSString *sourcePath=[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:file];
    [fileManager copyItemAtPath:sourcePath toPath:destinationPath error:&error];
}

+ (NSString *)getPathFromDocuments:(NSString *)file {
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectoryPath = [paths objectAtIndex:0];
    NSString *documentPath = [documentDirectoryPath stringByAppendingPathComponent:file];
    
    NSLog(@"File: %@", documentPath);
    
    return documentPath;
}

@end
