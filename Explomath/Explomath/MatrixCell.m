//
//  MatrixCell.m
//  Batalha Naval
//
//  Created by Matheus Cassol on 17/04/15.
//  Copyright (c) 2015 Matheus Cassol. All rights reserved.
//

#import "MatrixCell.h"

@implementation MatrixCell

NSString *const addition = @"addition";
NSString *const multiplication = @"multiplication";
NSString *const subtraction = @"subtraction";
NSString *const division = @"division";


- (id) initWithAnswer:(int) answer :(NSString *)andExpression :(NSString *)type {
    self = [super init];
    if (self) {
        _answer = answer;
        _expression = andExpression;
        _type = type;
        _isAnswered = NO;
        [self setCellImage];
    }
    return self;
}

- (void) setCellImage {
    if ([_type isEqualToString:@"normal"]) {
        _image = [_expression stringByAppendingString:@".png"];
    } else if ([_type isEqualToString:@"bomb"]) {
        _image = @"bomb.png";
    }
}

@end