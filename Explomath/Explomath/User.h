//
//  User.h
//  Explomath
//
//  Created by Cristian Simioni Milani on 21/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LevelStatus.h"

@interface User : NSObject{
    int stars[5];
}

@property LevelStatus *levelStatus;


+ (User *) sharedInstance;
-(void)setStars:(int)nStars InLevel:(int)lvl;
-(int*)getStars;

@end
