//
//  ConfigurationViewController.h
//  Explomath
//
//  Created by Cristian Simioni Milani on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfigurationViewController : UIViewController

// Select Language
@property (weak, nonatomic) IBOutlet UISegmentedControl *segCtrlLanguage;
// Turn sound on/of
@property (weak, nonatomic) IBOutlet UISwitch *swtSom;

@end
