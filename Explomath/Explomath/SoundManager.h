//
//  SoundManager.h
//  Explomath
//
//  Created by Victor Salvino Borges on 23/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@class Sound;

@interface SoundManager : NSObject

+(SoundManager *)sharedInstance;

- (void)configBackgroundSound;

- (void)setSoundOnOff;

- (void)playBackgroundSound:(BOOL)blPlay;

- (void)playRightSound:(BOOL)blPlay;

- (void)playWrongSound:(BOOL)blPlay;

- (void)playBombSound:(BOOL)blPlay;

- (void)playTimer:(BOOL)blPlay;

-(void)stopAllSoundsEffects;

@end
