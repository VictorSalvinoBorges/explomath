//
//  FileManager.h
//  Explomath
//
//  Created by Cristian Simioni Milani on 18/04/15.
//  Copyright (c) 2015 BCMV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject

+ (void)moveFromBundleToDocuments:(NSString *)file;

+ (NSString *)getPathFromDocuments:(NSString *)file;

@end
