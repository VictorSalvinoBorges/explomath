//
//  AppDelegate.h
//  Batalha Naval
//
//  Created by Matheus Cassol on 15/04/15.
//  Copyright (c) 2015 Matheus Cassol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

